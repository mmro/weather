package com.micmr0.weather

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_weather.*


class WeatherActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weather)

        val weather = intent.getParcelableExtra<RotObject>("WEATHER")

        Picasso.get().load("http://openweathermap.org/img/wn/" + weather.weather?.get(0)?.icon + ".png")
            .into(weather_icon)

        val temp = weather.main?.temp?.minus(273.15)

        city_name.text = String.format("%s, %s", weather.name, weather.sys?.country)
        temperature.text = String.format(
            "%1.2f°C , %s",
            temp,
            weather.weather?.get(0)?.main
        )

        when {
            temp!! < 10 -> {
                temperature.setTextColor(resources.getColor(R.color.colorPrimaryDark))
            }
            temp < 10 -> {
                temperature.setTextColor(resources.getColor(R.color.normal_temp))
            }
            else -> {
                temperature.setTextColor(resources.getColor(R.color.high_temp))
            }
        }

        feels.text = getString(R.string.feels, weather.main!!.feelsLike)
        humidity.text = getString(R.string.humidity, weather.main!!.humidity)
        pressure.text = getString(R.string.pressure, weather.main!!.pressure)

        api.setOnClickListener {
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(getString(R.string.weather_api_url))
            startActivity(i)
        }
    }
}