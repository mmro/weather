package com.micmr0.weather

import android.os.Parcel
import android.os.Parcelable

class Clouds() : Parcelable {
    var all = 0

    constructor(parcel: Parcel) : this() {
        all = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(all)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Clouds> {
        override fun createFromParcel(parcel: Parcel): Clouds {
            return Clouds(parcel)
        }

        override fun newArray(size: Int): Array<Clouds?> {
            return arrayOfNulls(size)
        }
    }
}

class Main() : Parcelable {
    var temp = 0.0
    var feelsLike = 0.0
    var tempMin = 0.0
    var tempMax = 0.0
    var pressure = 0
    var humidity = 0

    constructor(parcel: Parcel) : this() {
        temp = parcel.readDouble()
        feelsLike = parcel.readDouble()
        tempMin = parcel.readDouble()
        tempMax = parcel.readDouble()
        pressure = parcel.readInt()
        humidity = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(temp)
        parcel.writeDouble(feelsLike)
        parcel.writeDouble(tempMin)
        parcel.writeDouble(tempMax)
        parcel.writeInt(pressure)
        parcel.writeInt(humidity)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Main> {
        override fun createFromParcel(parcel: Parcel): Main {
            return Main(parcel)
        }

        override fun newArray(size: Int): Array<Main?> {
            return arrayOfNulls(size)
        }
    }

}

class RotObject() : Parcelable {
    var coord: Coord? = null
    var weather: Array<Weather>? = null
    var base: String? = null
    var main: Main? = null
    var visibility = 0
    var wind: Wind? = null
    var clouds: Clouds? = null
    var dt = 0
    var sys: Sys? = null
    var timezone = 0
    var id = 0
    var name: String? = null
    var cod = 0

    constructor(parcel: Parcel) : this() {
        weather = parcel.createTypedArray(Weather)
        base = parcel.readString()
        main = parcel.readParcelable(Main::class.java.classLoader)
        visibility = parcel.readInt()
        wind = parcel.readParcelable(Wind::class.java.classLoader)
        clouds = parcel.readParcelable(Clouds::class.java.classLoader)
        dt = parcel.readInt()
        sys = parcel.readParcelable(Sys::class.java.classLoader)
        timezone = parcel.readInt()
        id = parcel.readInt()
        name = parcel.readString()
        cod = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedArray(weather, flags)
        parcel.writeString(base)
        parcel.writeParcelable(main, flags)
        parcel.writeInt(visibility)
        parcel.writeParcelable(wind, flags)
        parcel.writeParcelable(clouds, flags)
        parcel.writeInt(dt)
        parcel.writeParcelable(sys, flags)
        parcel.writeInt(timezone)
        parcel.writeInt(id)
        parcel.writeString(name)
        parcel.writeInt(cod)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RotObject> {
        override fun createFromParcel(parcel: Parcel): RotObject {
            return RotObject(parcel)
        }

        override fun newArray(size: Int): Array<RotObject?> {
            return arrayOfNulls(size)
        }
    }

}

class Sys() : Parcelable {
    var type = 0
    var id = 0
    var country: String? = null
    var sunrise = 0
    var sunset = 0

    constructor(parcel: Parcel) : this() {
        type = parcel.readInt()
        id = parcel.readInt()
        country = parcel.readString()
        sunrise = parcel.readInt()
        sunset = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(type)
        parcel.writeInt(id)
        parcel.writeString(country)
        parcel.writeInt(sunrise)
        parcel.writeInt(sunset)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Sys> {
        override fun createFromParcel(parcel: Parcel): Sys {
            return Sys(parcel)
        }

        override fun newArray(size: Int): Array<Sys?> {
            return arrayOfNulls(size)
        }
    }

}

class Weather() : Parcelable {
    var id = 0
    var main: String? = null
    var description: String? = null
    var icon: String? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readInt()
        main = parcel.readString()
        description = parcel.readString()
        icon = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(main)
        parcel.writeString(description)
        parcel.writeString(icon)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Weather> {
        override fun createFromParcel(parcel: Parcel): Weather {
            return Weather(parcel)
        }

        override fun newArray(size: Int): Array<Weather?> {
            return arrayOfNulls(size)
        }
    }

}

class Wind() : Parcelable {
    var speed = 0.0
    var deg = 0

    constructor(parcel: Parcel) : this() {
        speed = parcel.readDouble()
        deg = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(speed)
        parcel.writeInt(deg)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Wind> {
        override fun createFromParcel(parcel: Parcel): Wind {
            return Wind(parcel)
        }

        override fun newArray(size: Int): Array<Wind?> {
            return arrayOfNulls(size)
        }
    }

}