package com.micmr0.weather

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView.OnItemClickListener
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.*
import java.io.*
import java.lang.reflect.Type
import java.util.*

class MainActivity : AppCompatActivity() {
    private var weather: RotObject? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        try {
            val cities: ArrayList<RootObject> = readJson()
            val cityNames = ArrayList<String>()
            for (city in cities) {
                cityNames.add(city.name!!)
            }
            val adapter = ArrayAdapter(this, android.R.layout.select_dialog_singlechoice, cityNames)
            val acTextView = findViewById<AutoCompleteTextView>(R.id.city_text_view)
            acTextView.threshold = 1
            acTextView.setAdapter(adapter)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        val listView =
            findViewById<View>(R.id.history_list) as ListView

        val history =
            getPreferences(Context.MODE_PRIVATE).getStringSet("HISTORY", null)

        var values: List<String> = ArrayList<String>()
        if (history != null) {
            values = ArrayList<String>(history)
        }

        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
            this,
            android.R.layout.simple_list_item_1, android.R.id.text1, values
        )
        listView.adapter = adapter

        listView.onItemClickListener =
            OnItemClickListener { _, _, position, _ ->
                city_text_view.setText(listView.adapter.getItem(position) as String)
                search_button.callOnClick()
            }

        search_button.setOnClickListener {
            val sharedPref =
                getPreferences(Context.MODE_PRIVATE)
            var history =
                sharedPref.getStringSet("HISTORY", null)
            if (history == null) {
                history = HashSet()
            }

            history.add(city_text_view.text.toString())
            sharedPref.edit().putStringSet("HISTORY", history).apply()
            getHttpResponse(city_text_view.text.toString())
        }

    }

    private fun getHttpResponse(city: String): RotObject? {
        val url =
            "http://api.openweathermap.org/data/2.5/weather?q=$city&appid=f2fa64f2fcff80f571c53d82bda2b784"
        val client = OkHttpClient()
        val builder = Request.Builder()
            .url(url)
            .header("Accept", "application/json")
            .header("Content-Type", "application/json")
            .method("GET", null)
            .build()
        client.newCall(builder).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.d("MainActivity", "onFailure: " + e.message)
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                Log.d("MainActivity", "onResponse")
                if (!response.isSuccessful) {
                    Log.d("MainActivity", "!response.isSuccessful()")
                    throw IOException("Unexpected code")
                } else {
                    val message = response.body()!!.string()
                    Log.d("MainActivity", "repo is: " + response.body().toString())
                    val gson = Gson()
                    val responseBody = client.newCall(builder).execute().body()
                    weather = gson.fromJson(responseBody!!.string(), RotObject::class.java)

                    val intent = Intent(this@MainActivity, WeatherActivity::class.java)
                    intent.putExtra("WEATHER", weather)
                    startActivity(intent)
                }
            }
        })
        return weather
    }

    @Throws(IOException::class)
    private fun readJson(): ArrayList<RootObject> {
        val `is` = resources.openRawResource(R.raw.city)
        val writer: Writer = StringWriter()
        val buffer = CharArray(1024)
        try {
            val reader: Reader = BufferedReader(InputStreamReader(`is`, "UTF-8"))
            var n: Int
            while (reader.read(buffer).also { n = it } != -1) {
                writer.write(buffer, 0, n)
            }
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            `is`.close()
        }
        val jsonString = writer.toString()
        val fooType: Type = object : TypeToken<ArrayList<RootObject?>?>() {}.type
        return Gson().fromJson(jsonString, fooType)
    }
}